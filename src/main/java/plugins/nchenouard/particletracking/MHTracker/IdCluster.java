package plugins.nchenouard.particletracking.MHTracker;

/** 
 * Simple integer object
 * 
 * Part of the Spot Tracking plugin for ICY: http://icy.bioimageanalysis.org/plugin/Spot_Tracking
 * 
 * @author Nicolas Chenouard (nicolas.chenouard@gmail.com).
 * @version 3.1
 * date 2013-11-13
 * license gpl v3.0
 * */

public class IdCluster
{
	int id;
	public IdCluster(int id)
	{
		this.id = id;
	}
}
