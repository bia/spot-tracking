package plugins.nchenouard.particletracking.MHTracker.LinearProgramming;

public class HierarchicalProcedureResult
{
	public double[] solution;
	public double score;
	public boolean hadNonBinary = false;
}